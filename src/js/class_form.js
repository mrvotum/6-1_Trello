import reloadList from './reloadList';

export default class AddForm {
  constructor(id) {
    this.trello = document.querySelector('#trello');
    this.id = id;
    this.addBtn = document.querySelector(`#addCard_${id}`);
  }

  create() {
    this.column = document.querySelector(`#columnList_${this.id}`);
    const formEl = document.createElement('form');
    formEl.setAttribute = ('data-id', 'form');
    formEl.innerHTML = `<textarea class="input_addCard" id="addCard_text" placeholder="Enter a title for this card..."></textarea>
    <input class="btn" type="submit" value="Add Card">
    <input class="reset" type="reset" value="×">`;
    this.column.appendChild(formEl);

    this.addBtn.style.display = 'none';
    this.addBtn.className = 'hide';

    this.addForm = document.querySelector('[data-id=form]');
    this.addFormSubmit = document.querySelector('[type=submit]');
    this.addFormReset = document.querySelector('[type=reset]');

    this.addSubmitListenter();
    this.addResetListenter();
  }

  addSubmitListenter() {
    this.addFormSubmit.addEventListener('click', (event) => {
      event.preventDefault();
      const cardHolder = document.querySelector(`#columnList_${this.id}`);
      const cardEl = document.createElement('li');
      cardEl.className = 'card';
      cardEl.draggable = true;
      cardEl.setAttribute = ('draggable', 'true');
      cardEl.textContent = document.querySelector('#addCard_text').value;

      cardEl.appendChild(this.addCrossBtn());
      cardHolder.appendChild(cardEl);

      const formId = this.column.childElementCount - 2;
      this.column.children[formId].remove();
      this.addBtn.style.display = 'block';
      this.addBtn.className = 'addCard';

      reloadList();
    });
  }

  addCrossBtn() {
    const spanEl = document.createElement('span');
    spanEl.className = 'cross';
    spanEl.textContent = '×';
    return spanEl;
  }

  addResetListenter() {
    this.addFormReset.addEventListener('click', (event) => {
      event.preventDefault();
      this.column.lastChild.remove();
      this.addBtn.style.display = 'block';
      this.addBtn.className = 'addCard';
    });
  }
}

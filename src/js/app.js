import AddForm from './class_form';
import reloadList from './reloadList';

reloadList();

const trello = document.querySelector('#trello');
let columnId = 0;

trello.addEventListener('click', (event) => {
  // клик для добавления новой карточки
  if (event.toElement.id.slice(0, 7) === 'addCard' && event.toElement.className === '') {
    columnId = event.toElement.id.slice(-1); // о какой колонке идёт речь

    const addForm = new AddForm(columnId);
    addForm.create();
  }

  if (event.toElement.className === 'cross') {
    event.toElement.parentElement.remove();
  }
});

// for (var i = 0; i < localStorage.length; i++) {
//   let key = localStorage.key(i);
//   console.log(key + " = " + localStorage.getItem(key));
// }

// запишем, что икс это два
// localStorage.setItem('key','value');
// прочитаем икс
// var x = localStorage.getItem('x');
// console.log(x); //выведет 2
// удалим ненужный объект
// localStorage.removeItem('x');
// localStorage.clear()
